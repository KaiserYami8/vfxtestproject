﻿Shader "Custom/Stencil"
{
	Properties{
		[IntRange] _Stencil ("Stencil Ref", Range(0, 255)) = 0
	}
	SubShader{
		Tags { "RenderType" = "Transparent" "Queue" = "Geometry-10" }
		Pass{
			Stencil{
				Ref [_Stencil]
				Comp Always
				Pass Replace
			}
		ColorMask 0
		Zwrite Off
		}
	}
}